# Knight mod

Mod for [Cataclysm: DDA](https://github.com/CleverRaven/Cataclysm-DDA/) that added "Knight" profession to "Medieval Peasant" scenario.

![screenshot1](https://user-images.githubusercontent.com/1105196/119635493-2fd18c80-be2d-11eb-846d-42a409c34ca6.png)
![screenshot2](https://user-images.githubusercontent.com/1105196/119635573-437cf300-be2d-11eb-8403-08d44e40fe77.png)
